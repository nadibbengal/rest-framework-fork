from django.db import models

class interest(models.Model):
    title=models.CharField(max_length=255)
    count = models.IntegerField()

    class Meta:
        ordering =('-count',)

    def __str__ (self):
        return  self.title
