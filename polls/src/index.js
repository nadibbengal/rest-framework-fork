import App from './components/App'
import ListApp from './listcomponent/ListApp'
import ReactDOM from 'react-dom'
import React from 'react'


var path=window.location.pathname
if(path=="/poll/" || path=="/"){
    ReactDOM.render(<ListApp/>,document.getElementById('questionList'));
}
else 
{
    ReactDOM.render(<App/>,document.getElementById('polls'));
}


