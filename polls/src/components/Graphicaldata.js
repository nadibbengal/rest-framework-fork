import React, { Component } from 'react'
import {Link} from 'react-router-dom'
export default class Graphicaldata extends Component {
    state={
        data:null
    }
    componentDidMount()
    {   
        var id=window.location.pathname.slice(6);
        fetch('http://localhost:8000/poll/'+id+'getdata').
        then(result =>{
           return result.json();
        }).then(data =>{
            console.log(data)
            this.setState(
                {
                    data:data
                }
            )
            var choice=data.choice
            var datas=data.datas
            var ctx = document.getElementById('myChart').getContext('2d');
            var myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: choice,
                datasets: [{
                    label: '# of Votes',
                    data: datas,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(60, 217, 86, 0.2)',
                        'rgba(255, 255, 0, 0.2)',
                        'rgba(0, 255, 204, 0.2)',
                        'rgba(255, 0, 102, 0.2)',
                        'rgba(102, 0, 204, 0.2)',
                        'rgba(0, 51, 0, 0.2)',
                        'rgba(255, 153, 0, 0.2)',
                        'rgba(255, 51, 204, 0.2)',
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(60, 217, 86, 1)',
                        'rgba(255, 255, 0, 1)',
                        'rgba(0, 255, 204, 1)',
                        'rgba(255, 0, 102, 1)',
                        'rgba(102, 0, 204, 1)',
                        'rgba(0, 51, 0, 1)',
                        'rgba(255, 153, 0, 1)',
                        'rgba(255, 51, 204, 1)',
                        
                    ],
                    borderWidth: 2
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
        })
    }
    render() {
        if(this.state.data){  
            return (
                <section>
                <canvas id="myChart"></canvas>
                </section>
            )
        
       
    }
    else
    {
        return (
            <div className="spinner-border" role="status">
            <span className="sr-only">Loading...</span>
            </div>
        )
    }
}
}
