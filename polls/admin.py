from django.contrib import admin
from .models import Question,Choice,VoteViewCounter
class ChoiceInline(admin.StackedInline):
    model = Choice
    extra = 3


class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['question_text','user','tag','music','poetry','painting']}),
    ]
    inlines = [ChoiceInline]
    list_display = ('question_text', 'pub_date','user','tag','total','music','poetry','painting','technology','vote')
class choiceAdmin(admin.ModelAdmin):
    list_display = ('choice_text', 'question','votes')
class counterAdmin(admin.ModelAdmin):
    list_display = ('Question',)
    filter_horizontal=('votedUser','viewedUser')

admin.site.register(VoteViewCounter,counterAdmin)
admin.site.register(Question,QuestionAdmin)
admin.site.register(Choice,choiceAdmin)