from rest_framework.serializers import ModelSerializer,HyperlinkedIdentityField,SerializerMethodField

from polls.models import Question,Choice

from profileinfo.models import interest


class PollCreateSerializers(ModelSerializer):
    class Meta:
        model = Question
        fields = [
            'question_text',
            'tag'
        ]



class PollListSerializers(ModelSerializer):
    url= HyperlinkedIdentityField(
        view_name='api:detail',
        lookup_field='id'
    )
    user=SerializerMethodField()
    tag=SerializerMethodField()
    class Meta:
        model = Question
        fields = [
            'id',
            'question_text',
            'url',
            'user',
            'tag',
              
        ]
    def get_user(self,obj):
        return  str(obj.user.username)
    
    def get_tag(self,obj):
        return str(obj.tag.title)

class ChoiceSerializer(ModelSerializer):
    class Meta:
        model = Choice
        fields = '__all__'


class PollDetailSerializers(ModelSerializer):
    choices = ChoiceSerializer(many=True)
    class Meta:
        model = Question
        fields = [
            'id',
            'question_text',
            'user',
            'total',
            'tag',
            'choices'
        ]



class InterestListSerializers(ModelSerializer):
    class Meta:
        model = interest
        fields = [
           'title',
           'id'
        ]

class ChoiceCreateSerializer(ModelSerializer):
    class Meta:
        model = Choice
        fields = [
            'question',
            'choice_text'
        ]