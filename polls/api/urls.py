from django.conf.urls import url
from .views import (
    QuestionlistAPIView,
    QuestionDetailAPIView,
    QuestionUpdateAPIView,
    QuestionDeleteAPIView,
    QuestionCreateAPIView,
    InterestListAPIView,
)


urlpatterns = [
    url(r'^$',QuestionlistAPIView.as_view(),name='list'),
    url(r'^create/$',QuestionCreateAPIView.as_view(),name='create'),
    url(r'^interest/$',InterestListAPIView.as_view(),name='interest'),
    url(r'^(?P<id>[\w-]+)/$',QuestionDetailAPIView.as_view(),name='detail'),
    url(r'^(?P<id>[\w-]+)/update/$',QuestionUpdateAPIView.as_view(),name='update'),
    url(r'^(?P<id>[\w-]+)/delete/$',QuestionDeleteAPIView.as_view(),name='delete'),
    
    # url(r'^profile$',views.profile,name='profile'),
    # url(r'^admin$',views.admin,name='admin'),
    # url(r'^createpolls$',views.createpolls,name='createpoll'),
    # # ex: /polls/5/
    # url(r'^(?P<question_id>[0-9]+)/$', views.detail, name='detail'),
    # # ex: /polls/5/results/
    # url(r'^(?P<question_id>[0-9]+)/results/$', views.results, name='results'),
    # # ex: /polls/5/vote/
    # url(r'^(?P<question_id>[0-9]+)/vote/$', views.vote, name='vote'),
    # url(r'^(?P<question_id>[0-9]+)/getdata/$', views.getdata, name='getdata'),
]   